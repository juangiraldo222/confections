<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Confecciones</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/favicon.png">

		<!-- all css here -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
       integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">

        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/owl.carousel.min.css">
        <!-- <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/ionicons.min.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/meanmenu.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/animate.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/bundle.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/slick.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/jquery-ui.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/nivo-slider.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/style.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/responsive.css">
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/vendor/modernizr-3.5.0.min.js"></script>
    </head>
    <body>
        <!-- Header Area Start -->
        <header class="header-area">
            <!-- Mainmenu Area Start -->
            <div class="mainmenu-area header-sticky fixed">
                <div class="full-container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5 display-none-md display-none-xs">
                            <div class="main-menu">
                                <nav>
                                    <ul>
                                        <li class="active"><a href="{{ url('/') }}">Inicio</a>
                                            <!-- <ul>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">Home Shop 1</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-2.html">Home Shop 2</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-3.html">Home Shop 3</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-4.html">Home Shop 4</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-5.html">Home Shop 5</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-6.html">Home Shop 6</a></li>
                                            </ul> -->
                                        </li>
                                        <!-- <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Shop</a>
                                            <ul>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-full-width.html">Shop Full Width</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-righ-sidebar.html">Shop Right Sidebar</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">Single Shop</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Cart Page</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout Page</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist Page</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog.html">Blog</a>
                                            <ul>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-2-column.html">Blog 2 Column</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-3-column.html">Blog 3 Column</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Blog Details</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details-left-sidebar.html">Blog Details Left Sidebar</a></li>
                                            </ul>
                                        </li>
                                        <li class="megamenu"><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Features</a>
                                            <ul>
                                                <li>
                                                    <ul>
                                                        <li>Special Pages</li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">About Us</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about-2.html">About Us 02</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service.html">Service</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service-2.html">Service 02</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact Us</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact-2.html">Contact Us 02</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li>Pages</li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio.html">Portfolio</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio-details.html">Single Portfolio</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/faq.html">Frequently Questions</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">My Account</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/404.html">Error 404</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li>Special Pages</li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-full-width.html">Shop Full Width</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-righ-sidebar.html">Shop Right Sidebar</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">Single Shop</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Cart Page</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout Page</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist Page</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li> -->
                                        <li><a href="{{ url('/contact') }}">Contáctenos</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-5">
                            <div class="logo text-center">
                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">
                                  <!-- <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/logo/logo-white.png" alt="NEGO"> -->
                                  <h3><span style="color: white;">Manufacturas Luce</span></h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-7">
                            <div class="header-content d-flex justify-content-end">
                                <div class="search-wrapper">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="icon ion-ios-search-strong"></i></a>
                                    <form action="#" class="search-form">
                                        <input type="text" placeholder="Enter your search...">
                                        <button type="button"><i class="icon ion-ios-search-strong"></i></button>
                                    </form>
                                </div>
                                <div class="cart-wrapper">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">
                                        <i class="icon ion-bag"></i>
                                        <span>2</span>
                                    </a>
                                    <div class="cart-item-wrapper">
                                        <div class="single-cart-item d-flex">
                                            <div class="cart-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/cart/1.jpg" alt=""></a>
                                            </div>
                                            <div class="cart-text-btn">
                                                <div class="cart-text">
                                                    <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Fashion Wear</a></h5>
                                                    <span class="cart-qty">Qty: 1</span>
                                                    <span class="cart-price">$115.00</span>
                                                </div>
                                                <button type="button"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                        <div class="single-cart-item d-flex">
                                            <div class="cart-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/cart/2.jpg" alt=""></a>
                                            </div>
                                            <div class="cart-text-btn">
                                                <div class="cart-text">
                                                    <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Kaoreet lobortis</a></h5>
                                                    <span class="cart-qty">Qty: 1</span>
                                                    <span class="cart-price">$95.00</span>
                                                </div>
                                                <button type="button"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                        <div class="cart-price-total d-flex justify-content-between">
                                            <span>Subtotal:</span>
                                            <span> $210.00</span>
                                        </div>
                                        <div class="cart-links">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">View cart</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="settings-wrapper">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-grid"></i></a>
                                    <div class="settings-content">
                                        <h4>Language:</h4>
                                        <ul>
                                            <li>
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/icon/flag-1.png" alt="">German</a>
                                            </li>
                                            <li>
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/icon/flag-2.png" alt="">French</a>
                                            </li>
                                        </ul>
                                        <h4>Currency:</h4>
                                        <ul>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">$ USD</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">€ EUR</a></li>
                                        </ul>
                                        <h4>My Account</h4>
                                        <ul>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Documentation</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">My Account</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Shopping Cart</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">Login</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mainmenu Area End -->
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="mobile-menu container">
                    <nav id="mobile-menu-active">
                        <ul class="menu-overflow">
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">HOME</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">Homepage One</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-2.html">Homepage Two</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-3.html">Homepage Three</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-4.html">Homepage Four</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-5.html">Homepage Five</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-6.html">Homepage Six</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Shop</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-full-width.html">Shop Full Width</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-righ-sidebar.html">Shop Right Sidebar</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">Single Shop</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Cart Page</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout Page</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist Page</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog.html">Blog</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-2-column.html">Blog 2 Column</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-3-column.html">Blog 3 Column</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Blog Details</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details-left-sidebar.html">Blog Details Left Sidebar</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Features</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">About Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about-2.html">About Us 02</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service.html">Service</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service-2.html">Service 02</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact-2.html">Contact Us 02</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio.html">Portfolio</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio-details.html">Single Portfolio</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/faq.html">Frequently Questions</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/404.html">Error 404</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">Account</a></li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- Mobile Menu Area End -->
        </header>
        <!-- Header Area End -->
	    <!-- Slider Area Start -->
        <div class="slider-area">
            <div id="slider">
                <img src="/images/8b.jpg" alt="slider-img" title="#caption1" />
                <img src="/images/7.jpg" alt="slider-img" title="#caption2" />
                <img src="/images/4.jpg" alt="slider-img" title="#caption3" />
                <!-- <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/slider/2.jpg" alt="slider-img" title="#caption2" /> -->
                <!-- <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/slider/1.jpg" alt="slider-img" title="#caption1" /> -->
                <!-- <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/slider/3.jpg" alt="slider-img" title="#caption3" /> -->
            </div>
            <div class="nivo-html-caption" id="caption2" >
                <div class="slider-progress"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 offset-xl-0 col-lg-8 offset-lg-1">
                            <div class="slider-text slide-two text-left">
                                <!-- <h5 class="wow slideInDown" data-wow-delay=".3s">-10% OFF THIS WEEK</h5> -->
                                <h1 class="wow zoomIn" data-wow-delay=".9s">Confección de prendas al por mayor</h1>
                                <p class="wow fadeIn" data-wow-delay="1.3s">Boxer, pijamas, camisetas para estampar<br>Excelente calidad y precios.</p>
                                <div class="slider-btn-wrapper">
                                    <div class="slider-btn">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Alguna duda?</a>
                                    </div>
                                    <div class="slider-btn">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Cotizar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nivo-html-caption" id="caption1">
                <div class="slider-progress"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="slider-text text-center">
                                <!-- <h5 class="wow slideInDown" data-wow-delay=".3s">SALE OFF THIS WEEK</h5> -->
                                <h1 class="wow zoomIn" data-wow-delay=".9s">Indumentaria de trabajo</h1>
                                <p class="wow fadeIn" data-wow-delay="1.3s">Diseñamos, a medida, prendas de presentación para su negocio</p>
                                <div class="slider-btn-wrapper">
                                    <div class="slider-btn">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Alguna duda?</a>
                                    </div>
                                    <div class="slider-btn">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Cotizar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nivo-html-caption" id="caption3" >
                <div class="slider-progress"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 offset-xl-0 col-lg-8 offset-lg-1">
                            <div class="slider-text slide-two text-left">
                                <!-- <h5 class="wow slideInDown" data-wow-delay=".3s">ONLY IN OUR STORE</h5> -->
                                <h1 class="wow zoomIn" data-wow-delay=".9s">Uniformes para guarderías, escuelas y colegios</h1>
                                <p class="wow fadeIn" data-wow-delay="1.3s">Diseñamos y fabricamos uniformes de gala y educación física,<br/> a bajo costo y con prendas de alta calidad</p>
                                <div class="slider-btn-wrapper">
                                    <div class="slider-btn">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Alguna duda?</a>
                                    </div>
                                    <div class="slider-btn">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Cotizar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	    <!-- Slider Area End -->
	    <!-- Banner Area Start -->
	    <div class="banner-area pt-20">
	        <div class="banner-container">
	            <div class="row">
	                <div class="col-lg-4 col-md-4">
                        <a class="banner-image" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                          <img src="/images/12.png" alt="" style="height: 360px;">
                        </a>
	                </div>
	                <div class="col-lg-4 col-md-4">
                        <a class="banner-image" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                          <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/2.jpg" alt=""></a>
	                </div>
	                <div class="col-lg-4 col-md-4">
                        <a class="banner-image" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/3.jpg" alt=""></a>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- Banner Area End -->
        <!-- Product Area Start -->
        <div class="product-area ptb-110">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Algunos de nuestros trabajos</h3>
                    <!-- <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum formas.</p> -->
                </div>
            </div>
            <div class="container">
                <!-- <div class="product-tab-list nav d-flex justify-content-center" role="tablist">
                    <a class="active" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#tab1" data-toggle="tab" role="tab" aria-selected="true" aria-controls="tab1">Clothing</a>
                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#tab2" data-toggle="tab" role="tab" aria-selected="false" aria-controls="tab2">Handbags</a>
                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#tab3" data-toggle="tab" role="tab" aria-selected="false" aria-controls="tab3">Shoes</a>
                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#tab4" data-toggle="tab" role="tab" aria-selected="false" aria-controls="tab4">Accessories</a>
                </div> -->
                <div class="custom-row">
                    <div class="tab-content">
                        <div class="tab-pane active show fade" id="tab1" role="tabpanel">
                            <div class="product-carousel owl-carousel carousel-style-one carousel-style-dot">
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <!-- <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"></a> -->
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/7.jpg" alt="">
                                            <!-- <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul> -->
                                            <!-- <div class="timer">
                                                <div data-countdown="2022/01/01" class="timer-grid"></div>
                                            </div> -->
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <!-- <span class="old-price">$45.00</span> -->
                                                <!-- <span class="new-price">$40.00</span> -->
                                                <span>$40.00</span>
                                            </div>
                                            <!-- <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" title="Add to cart">+ Add to cart</a>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/8.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$115.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/2.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/1.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/3.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/9.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Handbag</a></h5>
                                            <div class="pro-price">
                                                <span>$70.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/10.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Shoe</a></h5>
                                            <div class="pro-price">
                                                <span>$130.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/11.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount Bag</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/12.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$115.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab2" role="tabpanel">
                            <div class="product-carousel owl-carousel carousel-style-one carousel-style-dot">
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/4.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                            <div class="timer">
                                                <div data-countdown="2022/01/01" class="timer-grid"></div>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/5.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$45.00</span>
                                                <span class="new-price">$40.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/6.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$115.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/1.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/3.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/14.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$150.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/17.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/18.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/19.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount Watch</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab3" role="tabpanel">
                            <div class="product-carousel owl-carousel carousel-style-one carousel-style-dot">
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/20.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Watch</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/21.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                            <div class="timer">
                                                <div data-countdown="2022/01/01" class="timer-grid"></div>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/1.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$45.00</span>
                                                <span class="new-price">$40.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/18.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$50.00</span>
                                                <span class="new-price">$30.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/2.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/3.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/1.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/17.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Sunglass</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/16.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Watch</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab4" role="tabpanel">
                            <div class="product-carousel owl-carousel carousel-style-one carousel-style-dot">
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/17.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Sunglass</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/16.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Watch</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/4.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                            <div class="timer">
                                                <div data-countdown="2022/01/01" class="timer-grid"></div>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/5.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$45.00</span>
                                                <span class="new-price">$40.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/15.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/14.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/6.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$115.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/1.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/3.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Area End -->
        <!-- Banner Two Area Start -->
        <div class="banner-area pb-110">
            <div class="container">
                <div class="row custom-margin--10">
                    <div class="col-lg-6 col-md-6 custom-padding-10">
                        <div class="banner-image-wrap">
                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html" class="banner-image">
                              <img src="/images/9.jpg" alt="">
                              <!-- <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/6.jpg" alt=""> -->
                            </a>
                            <div class="banner-hover-text">
                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Camisetas de presentación</a></h5>
                                <!-- <span>Discover Now</span> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 custom-padding-10">
                        <div class="row custom-margin--10">
                            <div class="col-lg-6 col-md-6 custom-padding-10 mb-20">
                                <div class="banner-image-wrap">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html" class="banner-image">
                                      <!-- <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/7.jpg" alt=""> -->
                                      <img src="/images/10.jpg" alt="">
                                    </a>
                                    <div class="banner-hover-text">
                                        <h5><a href="">Pijamas al por mayor</a></h5>
                                        <!-- <span>Discover Now</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 custom-padding-10 mb-20">
                                <div class="banner-image-wrap">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html" class="banner-image"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/8.jpg" alt=""></a>
                                    <div class="banner-hover-text">
                                        <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Boxers al por mayor</a></h5>
                                        <!-- <span>Discover Now</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 custom-padding-10">
                                <div class="banner-image-wrap">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html" class="banner-image"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/9.jpg" alt=""></a>
                                    <div class="banner-hover-text">
                                        <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Uniformes</a></h5>
                                        <!-- <span>Discover Now</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 custom-padding-10">
                                <div class="banner-image-wrap">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html" class="banner-image"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/10.jpg" alt=""></a>
                                    <div class="banner-hover-text">
                                        <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Otros</a></h5>
                                        <!-- <span>Discover Now</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner Two Area End -->
        <!-- Featured Latest Product Area Start -->
        <!-- <div class="featured-latest-product-area pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="subtitle">
                            <h3>Latest Arrivals</h3>
                        </div>
                        <div class="row">
                            <div class="product-carousel-2 owl-carousel carousel-style-dot-2">
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/17.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Sunglass</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/16.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Watch</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/13.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/12.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$45.00</span>
                                                <span class="new-price">$40.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/11.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/10.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/9.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$115.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/8.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/7.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="subtitle">
                            <h3>Featured Products</h3>
                        </div>
                        <div class="row">
                            <div class="product-carousel-2 owl-carousel carousel-style-dot-2">
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/19.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/21.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Shoes</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/14.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                            <div class="pro-price">
                                                <span>$115.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/15.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Sunglass</a></h5>
                                            <div class="pro-price">
                                                <span>$300.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/18.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">New Watch</a></h5>
                                            <div class="pro-price">
                                                <span>$170.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <span class="onsale">Sale!</span>
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/20.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$90.00</span>
                                                <span class="new-price">$80.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/12.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Fashion</a></h5>
                                            <div class="pro-price">
                                                <span class="old-price">$45.00</span>
                                                <span class="new-price">$40.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/18.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$100.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col">
                                    <div class="single-product-item">
                                        <div class="product-image">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">
                                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/7.jpg" alt="">
                                            </a>
                                            <ul class="product-hover">
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to Wishlist" ><i class="ion-android-favorite-outline"></i></a>
                                                </li>
                                                <li>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" data-toggle="tooltip" data-placement="left" title="Add to compare" ><i class="icon ion-android-options"></i></a>
                                                </li>
                                                <li class="btn-quickview modal-view" data-toggle="modal" data-target="#productModal">
                                                    <span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="icon ion-android-open"></i></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product-text">
                                            <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-shirt</a></h5>
                                            <div class="pro-price">
                                                <span>$60.00</span>
                                            </div>
                                            <div class="cart-btn-wrap">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html" data-toggle="tooltip" data-placement="top" title="Add to cart">+ Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Featured Latest Product Area End -->
	    <!-- Banner Two Area Start -->
	    <div class="banner-two-area pb-110 fix">
	        <div class="banner-container-2">
	            <div class="row">
	                <div class="col-lg-6 col-md-6">
                        <a class="banner-image" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/4.jpg" alt=""></a>
	                </div>
	                <div class="col-lg-6 col-md-6">
                        <a class="banner-image" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/banner/5.jpg" alt=""></a>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- Banner Area End -->
        <!-- Small Product Blog Area Start -->
        <!-- <div class="small-product-blog-area pb-110">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="subtitle">
                                    <h3>Top Rated</h3>
                                </div>
                                <div class="small-product-carousel owl-carousel carousel-style-dot-2">
                                    <div class="small-product-wrapper">
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/12.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Shoes</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$100.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/13.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Casual Shoes</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span class="old-price">$68.00</span>
                                                    <span class="new-price">$65.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/14.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wears</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$55.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="small-product-wrapper">
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/9.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashionable Bag</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$100.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/16.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer Shoes</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span class="old-price">$68.00</span>
                                                    <span class="new-price">$65.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/17.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Winter Sunglass</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$55.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="subtitle">
                                    <h3>On Sale</h3>
                                </div>
                                <div class="small-product-carousel owl-carousel carousel-style-dot-2">
                                    <div class="small-product-wrapper">
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/21.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount Shoes</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span class="old-price">$68.00</span>
                                                    <span class="new-price">$65.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/20.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Wrist Watch</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$100.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/19.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Branded Watch</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$55.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="small-product-wrapper">
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/18.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Fashion Wear</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$100.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/11.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Discount Bag</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span class="old-price">$68.00</span>
                                                    <span class="new-price">$65.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-small-product">
                                            <div class="s-product-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/2.jpg" alt=""></a>
                                            </div>
                                            <div class="product-text">
                                                <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Summer T-Shirt</a></h5>
                                                <div class="rating">
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" class="yellow"><i class="ion-ios-star"></i></a>
                                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-ios-star"></i></a>
                                                </div>
                                                <div class="pro-price">
                                                    <span>$55.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-12 blog-container">
                        <div class="subtitle">
                            <h3>From Our Blog</h3>
                        </div>
                        <div class="blog-carousel owl-carousel carousel-style-dot-2">
                            <div class="blog-item-wrapper">
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/blog/1.jpg" alt="">
                                            <span>Mar<span>10</span></span>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <h3><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Blog image post</a></h3>
                                        <div class="blog-post-info">
                                            <span>By</span>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">admin,</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">3 comments</a>
                                        </div>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero</p>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Read More</a>
                                    </div>
                                </div>
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/blog/2.jpg" alt="">
                                            <span>Dec<span>01</span></span>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <h3><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Post with Gallery</a></h3>
                                        <div class="blog-post-info">
                                            <span>By</span>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">admin,</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">3 comments</a>
                                        </div>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero</p>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-item-wrapper">
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/blog/3.jpg" alt="">
                                            <span>Aug<span>16</span></span>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <h3><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Post with Audio</a></h3>
                                        <div class="blog-post-info">
                                            <span>By</span>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">admin,</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">10 comments</a>
                                        </div>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero</p>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Read More</a>
                                    </div>
                                </div>
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/blog/4.jpg" alt="">
                                            <span>Oct<span>22</span></span>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <h3><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Post with Video</a></h3>
                                        <div class="blog-post-info">
                                            <span>By</span>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">admin,</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">17 comments</a>
                                        </div>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero</p>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-item-wrapper">
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/blog/5.jpg" alt="">
                                            <span>Apr<span>10</span></span>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <h3><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Fashion Event's</a></h3>
                                        <div class="blog-post-info">
                                            <span>By</span>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">admin,</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">22 comments</a>
                                        </div>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero</p>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Read More</a>
                                    </div>
                                </div>
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">
                                            <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/blog/6.jpg" alt="">
                                            <span>Dec<span>01</span></span>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <h3><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Summer Styles</a></h3>
                                        <div class="blog-post-info">
                                            <span>By</span>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">admin,</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">70 comments</a>
                                        </div>
                                        <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero</p>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Small Product Blog Area End -->
        <!-- Newsletter Area Start -->
        <!-- <div class="newsletter-area border-top ptb-110">
            <div class="container">
                <div class="section-title-2 text-center">
                    <span><span class="text-red">Special Offers</span> For Subscribers</span>
                    <h3>Ten Percent Member Discount</h3>
                    <p>Subscribe to our newsletters now and stay up to date with new collections, the latest lookbooks and exclusive offers.</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-9">
                        <div class="newsletter-form mc_embed_signup">
                            <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll" class="mc-form">
                                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Email Address" required>

                                    <div class="mc-news" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                                    <button id="mc-embedded-subscribe" type="submit" name="subscribe">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Newsletter Area End -->
        <!-- Footer Area Start -->
        <footer class="footer-area bg-dark-1 pt-90">
            <div class="container">
                <div class="footer-top pb-90">

                      <div class="row justify-content-md-center">
                          <div class="col col-lg-2">
                          </div>
                          <div class="col-md-auto">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">Acerca de nosotros</h4>
                                <p>Realizamos manufactura prendas para todo tipo de negocios. Enviamos a todo el país.</p>

                            </div>
                            <div class="single-footer-widget social-container">
                                <h4 class="footer-title">Siguenos:</h4>
                                <div class="social-link">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="instagram">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <!-- <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="linkedin">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="rss">
                                        <i class="fa fa-rss"></i>
                                    </a> -->
                                </div>
                            </div>
                          </div>
                          <div class="col col-lg-2">
                          </div>
                        </div>
                        <!-- <div class="col-xl-5 col-lg-4 col-md-12">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">Acerca de nosotros</h4>
                                <p>Realizamos manufactura prendas para todo tipo de negocios. Enviamos a todo el país.</p>

                            </div>
                            <div class="single-footer-widget social-container">
                                <h4 class="footer-title">Follow Us On Social:</h4>
                                <div class="social-link">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="instagram">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="linkedin">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="rss">
                                        <i class="fa fa-rss"></i>
                                    </a>
                                </div>
                            </div>
                        </div> -->

                        <!-- <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">Information</h4>
                                <ul class="footer-list">
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">About Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Partners</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Become an affiliate</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Careers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">My account</h4>
                                <ul class="footer-list">
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">My Account</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Order Tracking</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Shipping Information</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Return Policy</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">Popular Tags</h4>
                                <ul class="footer-tags">
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">accesories</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">blouse</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">clothes</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">digital</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">fashion</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">handbag</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">laptop</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">men</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">women</a></li>
                                </ul>
                            </div>
                        </div> -->

                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <div class="footer-text">
                                <span>Copyright &copy; 2018 <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Nego</a>. Todos los derechos reservados</span>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6">
                            <div class="payment-img justify-content-end d-flex">
                                <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/payment.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End -->

        <!-- QUICKVIEW PRODUCT -->
        <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <div class="quick-view-container">
                        <div class="column-left">
                            <div class="tab-content product-details-large" id="myTabContent">
                                <div class="tab-pane fade show active" id="single-slide1" role="tabpanel" aria-labelledby="single-slide-tab-1">
                                    <div class="single-product-img">
                                        <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="single-slide2" role="tabpanel" aria-labelledby="single-slide-tab-2">
                                    <div class="single-product-img">
                                        <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/2.jpg" alt="">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="single-slide3" role="tabpanel" aria-labelledby="single-slide-tab-3">
                                    <div class="single-product-img">
                                        <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="single-slide4" role="tabpanel" aria-labelledby="single-slide-tab-4">
                                    <div class="single-product-img">
                                        <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/4.jpg" alt="">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="single-slide5" role="tabpanel" aria-labelledby="single-slide-tab-5">
                                    <div class="single-product-img">
                                        <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/5.jpg" alt="">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="single-slide6" role="tabpanel" aria-labelledby="single-slide-tab-6">
                                    <div class="single-product-img">
                                        <img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/6.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="single-product-menu">
                                <div class="nav single-slide-menu" role="tablist">
                                    <div class="single-tab-menu">
                                        <a class="active" data-toggle="tab" id="single-slide-tab-1" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#single-slide1"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/s-1.jpg" alt=""></a>
                                    </div>
                                    <div class="single-tab-menu">
                                        <a data-toggle="tab" id="single-slide-tab-2" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#single-slide2"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/s-2.jpg" alt=""></a>
                                    </div>
                                    <div class="single-tab-menu">
                                        <a data-toggle="tab" id="single-slide-tab-3" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#single-slide3"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/s-3.jpg" alt=""></a>
                                    </div>
                                    <div class="single-tab-menu">
                                        <a data-toggle="tab" id="single-slide-tab-4" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#single-slide4"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/s-4.jpg" alt=""></a>
                                    </div>
                                    <div class="single-tab-menu">
                                        <a data-toggle="tab" id="single-slide-tab-5" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#single-slide5"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/s-5.jpg" alt=""></a>
                                    </div>
                                    <div class="single-tab-menu">
                                        <a data-toggle="tab" id="single-slide-tab-6" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#single-slide6"><img src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/product/view/s-6.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column-right">
                            <div class="quick-view-text">
                                <h2>Curabitur a purus</h2>
                                <h3 class="q-product-price">$115.00</h3>
                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">See all features</a>
                                <span>200 in stock</span>
                                <div class="input-cart">
                                    <input value="1" type="number">
                                    <button>Add to cart</button>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                <div class="share-product">
                                    <h4>Share this product</h4>
                                    <div class="social-link">
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="instagram">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                        <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="linkedin">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END QUICKVIEW PRODUCT -->

		<!-- all js here -->
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/popper.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/bootstrap.min.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/jquery.meanmenu.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/owl.carousel.min.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/jquery.nivo.slider.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/plugins.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/ajax-mail.js"></script>
        <script src="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/js/main.js"></script>
    </body>
</html>
