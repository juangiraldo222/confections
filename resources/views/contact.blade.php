
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Nego - Fashion Ecommerce Bootstrap 4 Template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/img/favicon.png">

		<!-- all css here -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
           integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">

        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/ionicons.min.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/meanmenu.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/animate.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/bundle.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/slick.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/jquery-ui.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/nivo-slider.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/style.css">
        <link rel="stylesheet" href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/assets/css/responsive.css">
        <script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
    </head>
    <body>

        <!-- Header Area Start -->
        <header class="header-area">
            <!-- Mainmenu Area Start -->
            <div class="mainmenu-area header-sticky fixed">
                <div class="full-container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5 display-none-md display-none-xs">
                            <div class="main-menu">
                                <nav>
                                    <ul>
                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">Home</a>
                                            <ul>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">Home Shop 1</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-2.html">Home Shop 2</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-3.html">Home Shop 3</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-4.html">Home Shop 4</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-5.html">Home Shop 5</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-6.html">Home Shop 6</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Shop</a>
                                            <ul>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-full-width.html">Shop Full Width</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-righ-sidebar.html">Shop Right Sidebar</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">Single Shop</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Cart Page</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout Page</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist Page</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog.html">Blog</a>
                                            <ul>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-2-column.html">Blog 2 Column</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-3-column.html">Blog 3 Column</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Blog Details</a></li>
                                                <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details-left-sidebar.html">Blog Details Left Sidebar</a></li>
                                            </ul>
                                        </li>
                                        <li class="megamenu"><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Features</a>
                                            <ul>
                                                <li>
                                                    <ul>
                                                        <li>Special Pages</li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">About Us</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about-2.html">About Us 02</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service.html">Service</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service-2.html">Service 02</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact Us</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact-2.html">Contact Us 02</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li>Pages</li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio.html">Portfolio</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio-details.html">Single Portfolio</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/faq.html">Frequently Questions</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">My Account</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/404.html">Error 404</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul>
                                                        <li>Special Pages</li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-full-width.html">Shop Full Width</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-righ-sidebar.html">Shop Right Sidebar</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">Single Shop</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Cart Page</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout Page</a></li>
                                                        <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist Page</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="active"><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contáctenos</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-5">
                            <div class="logo text-center">
                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html"><img src="assets/img/logo/logo-white.png" alt="NEGO"></a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-7">
                            <div class="header-content d-flex justify-content-end">
                                <div class="search-wrapper">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="icon ion-ios-search-strong"></i></a>
                                    <form action="#" class="search-form">
                                        <input type="text" placeholder="Enter your search...">
                                        <button type="button"><i class="icon ion-ios-search-strong"></i></button>
                                    </form>
                                </div>
                                <div class="cart-wrapper">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">
                                        <i class="icon ion-bag"></i>
                                        <span>2</span>
                                    </a>
                                    <div class="cart-item-wrapper">
                                        <div class="single-cart-item d-flex">
                                            <div class="cart-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html"><img src="assets/img/cart/1.jpg" alt=""></a>
                                            </div>
                                            <div class="cart-text-btn">
                                                <div class="cart-text">
                                                    <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Fashion Wear</a></h5>
                                                    <span class="cart-qty">Qty: 1</span>
                                                    <span class="cart-price">$115.00</span>
                                                </div>
                                                <button type="button"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                        <div class="single-cart-item d-flex">
                                            <div class="cart-img">
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html"><img src="assets/img/cart/2.jpg" alt=""></a>
                                            </div>
                                            <div class="cart-text-btn">
                                                <div class="cart-text">
                                                    <h5><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Kaoreet lobortis</a></h5>
                                                    <span class="cart-qty">Qty: 1</span>
                                                    <span class="cart-price">$95.00</span>
                                                </div>
                                                <button type="button"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                        <div class="cart-price-total d-flex justify-content-between">
                                            <span>Subtotal:</span>
                                            <span> $210.00</span>
                                        </div>
                                        <div class="cart-links">
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">View cart</a>
                                            <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="settings-wrapper">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><i class="ion-grid"></i></a>
                                    <div class="settings-content">
                                        <h4>Language:</h4>
                                        <ul>
                                            <li>
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><img src="assets/img/icon/flag-1.png" alt="">German</a>
                                            </li>
                                            <li>
                                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#"><img src="assets/img/icon/flag-2.png" alt="">French</a>
                                            </li>
                                        </ul>
                                        <h4>Currency:</h4>
                                        <ul>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">$ USD</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">€ EUR</a></li>
                                        </ul>
                                        <h4>My Account</h4>
                                        <ul>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Documentation</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">My Account</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Shopping Cart</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout</a></li>
                                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">Login</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mainmenu Area End -->
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="mobile-menu container">
                    <nav id="mobile-menu-active">
                        <ul class="menu-overflow">
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">HOME</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index.html">Homepage One</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-2.html">Homepage Two</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-3.html">Homepage Three</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-4.html">Homepage Four</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-5.html">Homepage Five</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/index-6.html">Homepage Six</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">Shop</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-full-width.html">Shop Full Width</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop-righ-sidebar.html">Shop Right Sidebar</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/product-details.html">Single Shop</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/cart.html">Cart Page</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/checkout.html">Checkout Page</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist Page</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog.html">Blog</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-2-column.html">Blog 2 Column</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-3-column.html">Blog 3 Column</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details.html">Blog Details</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/blog-details-left-sidebar.html">Blog Details Left Sidebar</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Features</a>
                                <ul>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">About Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about-2.html">About Us 02</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service.html">Service</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/service-2.html">Service 02</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact-2.html">Contact Us 02</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio.html">Portfolio</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/portfolio-details.html">Single Portfolio</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/faq.html">Frequently Questions</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/404.html">Error 404</a></li>
                                </ul>
                            </li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">Account</a></li>
                            <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- Mobile Menu Area End -->
        </header>
        <!-- Header Area End -->
        <!-- Breadcrumb Area Start -->
        <div class="breadcrumb-area bg-3 text-center">
            <div class="container">
                <h1>Contáctenos</h1>
                <nav aria-label="breadcrumb">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contáctenos</li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- Breadcrumb Area End -->
        <!-- Google Map Area Start -->
        <div class="google-map-area">
            <div id="contacts" class="map-area">
                <div id="googleMap" style="width:100%;height:400px;"></div>
            </div>
        </div>
        <!-- Google Map Area End -->
        <!-- Contact Area Start -->
        <div class="contact-area fix">
            <div class="contact-form pt-110">
                <h1 class="contact-title">TELL US YOUR PROJECT</h1>
                <form id="contact-form" action="mail.php" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="name" id="name" placeholder="First Name *">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="l_name" id="l_name" placeholder="Last Name *">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="email" id="email" placeholder="Email *">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="subject" id="subject" placeholder="Subject *">
                        </div>
                    </div>
                    <textarea name="message" id="message" placeholder="Message *"></textarea>
                    <button type="submit" class="submit-btn">
                        <span>Send Email</span>
                    </button>
                    <p class="form-message"></p>
                </form>
            </div>
            <div class="contact-address pt-110 pb-100">
                <h1 class="contact-title">CONTÁCTENOS</h1>
                <div class="contact-info">
                    <p>Escríbenos por <span style="font-weight: bold;">WhatsApp</span> para solicitar una cotización, o cualquier duda que nos quieras manifestar</p>
                    <div class="contact-list-wrapper">
                        <div class="contact-list">
                            <i class="fa fa-phone"></i>
                            <span>312 720 42 89</span>
                        </div>
                        <div class="contact-list">
                            <i class="fa fa-envelope-o"></i>
                            <span>confeccionesluce@gmail.com</span>
                        </div>
                        <div class="contact-list">
                            <i class="fa fa-fax"></i>
                            <span>Dirección : Cra 26 32-155 Marinilla, Antioquia</span>
                        </div>
                    </div>
                </div>
                <div class="working-time">
                    <h2>Horario de atención</h2>
                    <span><span>Lunes – Sábado:</span>  08 AM – 06 PM</span>
                </div>
            </div>
        </div>
        <!-- Contact Area End -->
        <!-- Newsletter Area Start -->
        <!-- Newsletter Area End -->
        <!-- Footer Area Start -->
        <footer class="footer-area bg-dark-1 pt-90">
            <div class="container">
                <div class="footer-top pb-90">
                    <div class="row">
                        <div class="col-xl-5 col-lg-4 col-md-12">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">About Us</h4>
                                <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum...</p>
                                <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">Readmore</a>
                            </div>
                            <div class="single-footer-widget social-container">
                                <h4 class="footer-title">Follow Us On Social:</h4>
                                <div class="social-link">
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="instagram">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="linkedin">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#" target="_blank" class="rss">
                                        <i class="fa fa-rss"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">Information</h4>
                                <ul class="footer-list">
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/about.html">About Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/contact.html">Contact Us</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Partners</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Become an affiliate</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Careers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">My account</h4>
                                <ul class="footer-list">
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/account.html">My Account</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/wishlist.html">Wishlist</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Order Tracking</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Shipping Information</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Return Policy</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div class="single-footer-widget">
                                <h4 class="footer-title">Popular Tags</h4>
                                <ul class="footer-tags">
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">accesories</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">blouse</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">clothes</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">digital</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">fashion</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">handbag</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">laptop</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">men</a></li>
                                    <li><a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/shop.html">women</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <div class="footer-text">
                                <span>Copyright &copy; 2018 <a href="https://d29u17ylf1ylz9.cloudfront.net/nego-v3/#">Nego</a>. Todos los derechos reservados</span>
                            </div>
                        </div>Todos los derechos reservados
                        <div class="col-xl-6 col-md-6">
                            <div class="payment-img justify-content-end d-flex">
                                <img src="assets/img/payment.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End -->

		<!-- all js here -->
        <script src="assets/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.meanmenu.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/jquery.nivo.slider.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/ajax-mail.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMlLa3XrAmtemtf97Z2YpXwPLlimRK7Pk"></script>
        <script>
            google.maps.event.addDomListener(window, 'load', init);
            function init() {
                var mapOptions = {
                    zoom: 11,
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York
                    styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                };
                var mapElement = document.getElementById('googleMap');
                var map = new google.maps.Map(mapElement, mapOptions);
                var marker = new google.maps.Marker({
                    position: map.getCenter(),
                    animation:google.maps.Animation.BOUNCE,
                    icon: 'assets/img/map-marker.png',
                    map: map
                });
            }
        </script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
